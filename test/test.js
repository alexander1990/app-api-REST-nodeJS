'use strict'
//alexander
var chaiHttp = require('chai-http');
let chai = require('chai');
chai.use(chaiHttp);
var expect  = require('chai').expect;
const app = require('../app');
const http = require('http').createServer(app).listen(3000);
var ProductCtrl = require('../controllers/product');
var product = require('../models/product');
var mongoose = require('mongoose');

var laptop = {
  "name": "hp",
  "picture": "hp.png",
  "price": 100,
  "category": 'computers', 
  "description": "laptop de la hp"
 };

describe('pruebas de HTTP', function() {

  describe('http', function () {
    before(function (done) {
      mongoose.connect('mongodb://localhost:27017/test');
      const db = mongoose.connection;
      db.on('error', console.error.bind(console, 'connection error'));
      db.once('open', function() {
        console.log('We are connected to test database!');
        done();
      });
    });

    it('prueba http', function (done) { 
      chai.request(http)
        .post('/api/ver')
        .send({'numero': 1})
        .end(function (err, res) {
          expect(err).to.be.null;
          expect(res).to.status(200);
          done()
       });
    });

    it('prueba find product', function (done) { 
      chai.request(http)
        .post('/api/product')
        .send(laptop)
        .end(function (err, res) {
          console.log(res.body.productStored.name);
          let id = res.body.productStored._id
          expect(err).to.be.null;
          expect(res).to.status(200);

          chai.request(http)
            .get(`/api/product/${id}`)
            .end(function(err, res) {
              console.log(res.body);
              expect(err).to.be.null;
              expect(res).to.status(200);
              done();
            })
        });
      });

      it('prueba all products', function (done) { 
        chai.request(http)
          .get('/api/product')
          .end(function (err, res) {
            console.log(res.body);
            expect(err).to.be.null;
            expect(res).to.status(200);
            done();
            
          });
      });

      it('prueba insert product', function (done) { 
        chai.request(http)
          .post('/api/product')
          .send(laptop)
          .end(function (err, res) {
            console.log(res.body.productStored.name);
            expect(err).to.be.null;
            expect(res).to.status(200);
            done(); 
          });
        });

    after(function(done){
      mongoose.connection.db.dropDatabase(function(){
        mongoose.connection.close(done);
      });
    });
 
  });
});





