'use strict'
var Product = require('../models/product');

function getProduct(req,res){
    let product_id = req.params.productId;
     Product.findById(product_id, (err, product)=>{
         if (err) return res.status(500).send({message:`hay un error en busqueda ${err}`});
         if (!product) return res.status(404).send({message:`producto no encontrado`});
         res.status(200).send({product});
          
     });
}

function getProducts(req,res){
    Product.find({},(err, products)=>{
        if (err) return res.status(500).send({message:`hay un error en busqueda ${err}`});
        if (!products) return res.status(404).send({message:`no hay prodcutos cargados`});
        res.status(200).send({products});
    });
}

function updateProduct(req,res){
    let product_id = req.params.productId;
    let body = req.body;
    console.log(product_id);
    Product.findByIdAndUpdate(product_id, body, (err,productUpdated)=>{
        if(err) return res.status(500).send({message: `Erro al borrar en la BD ${err}`});
        res.status(200).send({product: productUpdated});
    });
}

function delateProduct(req,res){
    let product_id = req.params.productId;
    console.log(product_id);
    Product.findById(product_id, (err, product)=>{
        if (err) return res.status(500).send({message:`hay un error en busqueda ${err}`});
        if (!product) return res.status(404).send({message:'el producto no existe'});
        product.remove(err =>{
            if(err) return res.status(500).send({message: `Erro al borrar en la BD ${err}`});
            res.status(200).send({message: `el producto ha sido borrado`});
        });         
    });
}

function saveProduct(req,res){
   /*console.log('metodo post api');
   console.log(req.body);*/
   let product = new Product(req.body);

   product.save((err, productStored)=>{
       if(err){
           res.status(500).send({message: `Erro BD ${err}`});
       }else{
        res.status(200).send({message: `Se guardo producto ${productStored}`,productStored});
       }
   });
}


function ver(req,res){
    res.status(200).send({message: 'exito'});
}

module.exports = {
    getProduct,
    getProducts,
    updateProduct,
    delateProduct,
    saveProduct,
    ver
}