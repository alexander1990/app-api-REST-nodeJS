'use strict'

const express = require('express');
const api = express.Router();
var ProductCtrl = require('../controllers/product');

api.post('/ver/', ProductCtrl.ver);
api.get('/product/', ProductCtrl.getProducts);
api.get('/product/:productId', ProductCtrl.getProduct);
api.post('/product/', ProductCtrl.saveProduct);
api.put('/product/:productId', ProductCtrl.updateProduct);
api.delete('/product/:productId', ProductCtrl.delateProduct);

module.exports = api;