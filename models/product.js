'use strict'

var mongoose = require('mongoose');
var schema = mongoose.Schema;


var ProductSchema = schema({
    name: String,
    picture: String,
    price: {type: Number, default: 0},
    category: {type: String, enum: ['computers','phones','accesories']}, 
    description: String
});

module.exports = mongoose.model('Product',ProductSchema);